#!/bin/bash

echo "Installing Node Modules..."
npm i --no-audit

echo "Entering SillyTavern..."
node "$(dirname "$0")/server.js"